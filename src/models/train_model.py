# импортируем trainer из pytorch_lightning и несколько классов оттуда
from pytorch_lightning import Trainer
# автоматическое логирование результатов
from pytorch_lightning.loggers import TensorBoardLogger
# сохранение весов
from pytorch_lightning.callbacks.model_checkpoint import ModelCheckpoint
# автоматическое отслеживание lr
from pytorch_lightning.callbacks import LearningRateMonitor
# ранние остановки
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
# импортируем нашу модель
from eff_net import EffNetClassifier

# импортируем дополнительные библиотеки
from datetime import datetime
from data_loader import ImgDataModule
from torchvision import transforms
import toml
import torch
import sys
sys.path.append(r'../data')


learning_conf = toml.load(r'../learning_conf.toml')
TRANSFORM_TRAIN = learning_conf['transform_train']
TRANSFORM_TEST = learning_conf['transform_test']
LEARNING_PARAMETERS = learning_conf['learning_parameters']
LOGGING_PARAMETERS = learning_conf['logging_parameters']
CHECKPOINT_PARAMETERS = learning_conf['checkpoint_parameters']
ES_PARAMETERS = learning_conf['early_stopping_parameters']
SAVING_PARAMETERS = learning_conf['saving_parameters']

# трансформации для обучения
transform_train = transforms.Compose(
    [transforms.Resize(TRANSFORM_TRAIN['resize']),
     transforms.RandomHorizontalFlip(p=TRANSFORM_TRAIN['rand_hor_flip']),
     transforms.ToTensor(),
     transforms.Normalize(mean=TRANSFORM_TRAIN['normalize_mean'],
                          std=TRANSFORM_TRAIN['normalize_std'])
     ]
)

# трансформации для валидации и теста
transform_test = transforms.Compose(
    [transforms.Resize(TRANSFORM_TEST['resize']),
     transforms.ToTensor(),
     transforms.Normalize(mean=TRANSFORM_TEST['normalize_mean'],
                          std=TRANSFORM_TEST['normalize_std'])
     ]
    )

# создаем DataModule-экземпляр
data_module = ImgDataModule(LEARNING_PARAMETERS['batch'],
                            transform_train, transform_test)
model = EffNetClassifier(eta=LEARNING_PARAMETERS['eta'],
                         model=LEARNING_PARAMETERS['experiment'])

# список для отслеживания lr, ранних остановок, сохранения весов
callbacks = [
    ModelCheckpoint(
        dirpath=CHECKPOINT_PARAMETERS['ckpt_dirpath'],
        filename=CHECKPOINT_PARAMETERS['filename'],
        save_top_k=CHECKPOINT_PARAMETERS['save_top_k'],
        monitor=CHECKPOINT_PARAMETERS['ckpt_monitor'],
        mode=CHECKPOINT_PARAMETERS['ckpt_mode']
        ),
    LearningRateMonitor(
        logging_interval=LOGGING_PARAMETERS['logging_interval']
        ),
    EarlyStopping(monitor=ES_PARAMETERS['es_monitor'],
                  min_delta=ES_PARAMETERS['min_delta'],
                  patience=ES_PARAMETERS['patience'],
                  verbose=ES_PARAMETERS['verbose'],
                  mode=ES_PARAMETERS['es_mode'])
    ]

# указываем логгер
logger = TensorBoardLogger(LOGGING_PARAMETERS['tensor'],
                           name=LEARNING_PARAMETERS['experiment'])

# создаем трейнер - класс, который будет обучать
trainer = Trainer(
        max_epochs=LEARNING_PARAMETERS['max_epochs'],
        logger=logger,
        callbacks=callbacks
)

try:
    trainer.fit(model,
                data_module,
                ckpt_path=CHECKPOINT_PARAMETERS['checkpoint'])
except FileNotFoundError:
    trainer.fit(model, data_module, ckpt_path=None)

torch.save(model, SAVING_PARAMETERS['saving_dirpath'])
