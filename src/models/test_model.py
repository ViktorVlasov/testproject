import pytorch_lightning as pl
import argparse
import torch
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from pathlib import Path
from tqdm.autonotebook import tqdm
from sklearn.metrics import classification_report

from eff_net import EffNetClassifier 
from src.data.data_loader import DistractedDriverDataset
from src.data.augmentations import DataAugmentations
from src.configs.base_config import combine_config
from src.configs.augmentations_config import get_torchvision_transforms, get_base_transforms, train_augmentations, \
    val_augmentations, test_augmentations

def get_data_module(batch_size, path_to_config):
    cfg = combine_config(path_to_config)

    base_transforms = get_torchvision_transforms(image_size=cfg.DATASET.IMG_SIZE)

    augmentations = DataAugmentations(base_transforms=base_transforms,
                                      train_augmentations=None,
                                      val_augmentations=None,
                                      test_augmentations=None,
                                      is_torch=True)

    data_module = DistractedDriverDataset(image_size=cfg.DATASET.IMG_SIZE,
                                          batch_size=batch_size,
                                          train_dir=cfg.DATASET.TRAIN_SUBFOLDER,
                                          val_dir=cfg.DATASET.VAL_SUBFOLDER,
                                          test_dir=cfg.DATASET.TEST_SUBFOLDER,
                                          num_workers=cfg.DATASET.NUM_WORKERS,
                                          train_shuffle=cfg.DATASET.SHUFFLE_TRAIN,
                                          val_shuffle=cfg.DATASET.SHUFFLE_VAL,
                                          test_shuffle=cfg.DATASET.SHUFFLE_TEST,
                                          augmentations=augmentations)
    
    return data_module


def test(model, dm, mode='test', device='cpu'):
    
    if mode == 'test':
        dataloader = dm.test_dataloader()
    elif mode == 'val':
        dataloader = dm.val_dataloader()

    predictions = []
    targets = []
    
    model.eval()
    with torch.no_grad():
        for X, y in tqdm(dataloader):
            outputs = torch.argmax(model(X.to(torch.float32).to(device)), 1)
            predictions.append(outputs.detach())
            targets.append(y.detach())        

    predictions = (torch.cat(predictions)).cpu().numpy() 
    targets = (torch.cat(targets)).cpu().numpy()
    classes = dm.test_set.classes
    
    print(classification_report(targets, predictions, target_names=classes))    


def get_args_parser():
    parser = argparse.ArgumentParser(description="Test model")
    parser.add_argument('--model', default='epoch=3_valid_acc=0.78_valid_loss=1.06.ckpt', type=str, help='Model name in models dir')
    parser.add_argument("--config", default='localpath_config.yaml', type=str)
    parser.add_argument("-b", "--batch_size", default=32, type=int, help="Number of images in a batch")
    parser.add_argument("--mode", default='test', type=str, help="Choosing a data loader: test or val Default: test")
    parser.add_argument("--device", default="cpu", type=str, help="Device (Use cuda or cpu Default: cpu)")
    
    return parser


if __name__ == "__main__":
    args = get_args_parser().parse_args()
    
    model_path = Path(__file__).parent / Path('../../models') / args.model
    path_to_config = Path('.') / '../configs' / args.config
    device = torch.device(args.device)
    model = EffNetClassifier.load_from_checkpoint(checkpoint_path=model_path).to(device)
    
    dm = get_data_module(32, path_to_config)
    dm.setup()
  
    test(model, dm)
    
  