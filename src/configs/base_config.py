"""
Базовый конфигурационный файл.
Предлагается использовать YACS config (https://github.com/rbgirshick/yacs) для поддержки базового конфигурационного
файла. Затем для каждого конкретного эксперимента можно создать конфигурационный файл, который будет переопределять
необходимые параметры базового конфигурационного файла.


Пример использования:
    1. Создать конфигурационный файл для конкретного эксперимента (например, configs/experiment_1.yaml)
    2. В конфигурационном файле переопределить необходимые параметры базового конфигурационного файла
    3. В модуле, где необходимо использовать конфигурационные параметры, импортировать функцию combine_config
    4. Вызвать функцию combine_config, передав в качестве аргумента путь к конфигурационному файлу конкретного эксперимента
    5. Полученный объект yacs CfgNode можно использовать для доступа к конфигурационным параметрам
"""

import os.path as osp
from typing import Union
from yacs.config import CfgNode as CN


_C = CN()

# Root directory of project
_C.ROOT = CN()
_C.ROOT.PATH = '/content/attention_control'  # Путь к корневой директории проекта в Google Colab

# Dataset
_C.DATASET = CN()
_C.DATASET.IMG_DIR = 'dataset_split'
_C.DATASET.TRAIN_SUBFOLDER = 'train'
_C.DATASET.VAL_SUBFOLDER = 'val'
_C.DATASET.TEST_SUBFOLDER = 'test'
_C.DATASET.IMG_SIZE = 224
_C.DATASET.BATCH_SIZE = 1
_C.DATASET.NUM_WORKERS = 2
_C.DATASET.SHUFFLE_TRAIN = True
_C.DATASET.SHUFFLE_VAL = False
_C.DATASET.SHUFFLE_TEST = False
_C.DATASET.IMG_DIR = osp.join(_C.ROOT.PATH, _C.DATASET.IMG_DIR)
_C.DATASET.TRAIN_SUBFOLDER = osp.join(_C.DATASET.IMG_DIR, _C.DATASET.TRAIN_SUBFOLDER)
_C.DATASET.VAL_SUBFOLDER = osp.join(_C.DATASET.IMG_DIR, _C.DATASET.VAL_SUBFOLDER)
_C.DATASET.TEST_SUBFOLDER = osp.join(_C.DATASET.IMG_DIR, _C.DATASET.TEST_SUBFOLDER)
_C.DATASET.CLASSES = ['c0 | safe driving', 
                    'c1 | texting - right', 
                    'c2 | talking on the phone - right', 
                    'c3 | texting - left',
                    'c4 | talking on the phone - left',
                    'c5 | operating the radio',
                    'c6 | drinking',
                    'c7 | reaching behind',
                    'c8 | hair and makeup',
                    'c9 | talking to passenger']


def get_cfg_defaults():
    """Возвращает yacs CfgNode объект со значениями по умолчанию"""
    return _C.clone()


def combine_config(cfg_path: Union[str, None] = None):
    """
    Объединяет базовый конфигурационный файл с конфигурационным файлом конкретного эксперимента
    Args:
         cfg_path (str): file in .yaml or .yml format with config parameters or None to use Base config
    Returns:
        yacs CfgNode object
    """
    base_config = get_cfg_defaults()
    if cfg_path is not None and osp.exists(cfg_path):
        base_config.merge_from_file(cfg_path)
    return base_config
